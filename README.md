To run
Install NodeJS from **Nodejs.org**

give following commands after getting into project

```
#!JavaScript

npm install grunt-cli -g
npm install bower -g
npm install
bower install
```


for server, give following command and goto localhost:9000

```
#!JavaScript

 grunt serve
```


for building attributable/deploy able final project, it will go in **dist** folder.

```
#!javascript

grunt build 
```