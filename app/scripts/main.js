/*global document, $, window, console, alert, Waypoint, notify*/
(function () {
    'use strict';

    $(document).ready(function () {
        $('#fullpage').fullpage({
            scrollBar: true,
            autoScrolling: false
        });
    });

    $("#slider-content").slick({
      "autoplay": true,
      "arrows": false
    });

    $(window).scroll(function () {
        $('#art-1').each(function () {
            var imagePos = $(this).offset().top,
                topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow + 400) {
                $(this).addClass('slideRight');
            }
        });

        $('#art-2').each(function () {
            var imagePos = $(this).offset().top,
                topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow + 400) {
                $(this).addClass('slideLeft');
            }
        });

        $('#art-text').each(function () {
            var imagePos = $(this).offset().top,
                topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow + 600) {
                $(this).addClass('hatch');
            }
        });

        $('#art-portfolio-heading').each(function () {
            var imagePos = $(this).offset().top,
                topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow + 650) {
                $(this).addClass('hatch');
            }
        });

        /* Developer Section */
        $('#left-eng').each(function () {
            var imagePos = $(this).offset().top,
                topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow + 400) {
                $(this).addClass('slideRight');
            }
        });

        $('#right-eng').each(function () {
            var imagePos = $(this).offset().top,
                topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow + 400) {
                $(this).addClass('slideLeft');
            }
        });

//        var fistWay = new Waypoint({
//            element: document.getElementById('slider-section'),
//            handler: function () {
//                console.log($("#rocket"));
//                $('#rocket').css('top', '70%');
//            }
//        }), secondWay = new Waypoint({
//            element: document.getElementById('slider'),
//            handler: function () {
//                console.log($("#rocket"));
//                $('#rocket').css('top', '50%');
//            }
//        }), thirdWay = new Waypoint({
//            element: document.getElementById('shuttle-2'),
//            handler: function () {
//                console.log($("#rocket"));
//                $('#rocket').css('top', '30%');
//            }
//        });

    });
}());


(function($) {
    $.expr[":"].onScreen = function(elem) {
        var $window = $(window);
        var viewport_top = $window.scrollTop();
        var viewport_height = $window.height();
        var viewport_bottom = viewport_top + viewport_height;
        var $elem = $(elem);
        var top = $elem.offset().top;
        var height = $elem.height();
        var bottom = top + height;

        return (top >= viewport_top && top < viewport_bottom) ||
            (bottom > viewport_top && bottom <= viewport_bottom) ||
            (height > viewport_height && top <= viewport_top && bottom >= viewport_bottom)
    }
})(jQuery);

setInterval(function () {
    if ($("#dev-section:onScreen").length == 1) {
        console.log("#div is visible");
    } else {
        console.log("#div is invisible");
    }
}, 1000);
